---
title: "La fattoria"
date: 2024-02-09T14:50:52+01:00
draft: false
---

La fattoria di Jallinières è attualmente composta da diversi edifici:
- un'**abitazione** di 150 m2 , dove inizialmente vivremo, ma che gradualmente trasformeremo in uno spazio collettivo: sala riunioni, biblioteca condivisa, cucina comune, dormitorio per ospitare i visitatori;
- un **granaio** in adobe di 110 m2 per lo stoccaggio e lo smistamento e la macinazione dei cereali;
- una **stalla**, una **sala di mungitura** e un **caseificio**, i cui 300 m2 saranno completamente ristrutturati e condivisi tra lo stoccaggio, il lavaggio e il confezionamento delle verdure e il panificio;
- un'area di 1000 m2 adiacente agli edifici esistenti, dove verrà costruito un nuovo edificio.

Oltre agli edifici, il cuore di ogni fattoria sono pur sempre i terreni coltivabili. L'azienda agricola comprende circa 47 ettari, di cui 15 ettari di proprietà del cedente e 32 ettari in affitto. Il cedente ci venderà gradualmente i suoi terreni; al momento ci stiamo assicurando che gli altri contratti di affitto vengano rinnovati per permetterci di insediarci.
