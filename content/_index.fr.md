---
title: "Accueil"
date: 2024-02-09T14:50:52+01:00
draft: false
---

# Soutenez la reprise d'une ferme en collectif !

Nous voulons faire vivre un lieu agricole collectif à <b>Saint-Ondras</b> dans le Nord Isère, sur la ferme des Jallinières. À sept, nous portons un projet d'<b>agriculture paysanne</b>, ancrée dans son territoire.  Après plus de trois ans de réflexions, construction et dé-construction, essais et apprentissages, notre collectif se lance à fond dans cette aventure extraordinaire !

|![alt text](/images/neige.jpg "La ferme des Jalinières sous la neige")|
|:--:|
| <p class="caption">La ferme des Jallinières sous la neige</p> |

## C'est quoi ?

La <b>ferme des Jallinières</b> a arrêté son activité de vaches laitières en 2021 : tout est à transformer et à réinventer. L'<b>achat</b> de la ferme est la première étape pour permettre la création de plusieurs activités agricoles et d'un lieu associatif de rencontres et d'échanges (autours de chantiers participatifs au début, et de plein d'autres choses à terme). 

|![alt text](/images/croquis_gris.jpg "Croquis de la ferme")|
|:--:|
| <p class="caption">La projection des activités sur la ferme</p> |

## Avec qui ?

Les <b>grandes cultures</b> (céréales) seront la première activité à être lancée, avec sa <b>meunerie</b> et sa <b>boulangerie</b> : Clément et Florentin ont déjà semé leurs premiers hectares cet automne ! Courant 2024, l'activité <b>maraîchage</b> sera lancée sur les parcelles juste en face de la ferme, par Emma, Jacob et Nathalie. Ensuite, pour l'activité <b>petits fruits</b> d'Eva et ses délicieuses confitures, plein de haies et d'arbres fruitiers seront plantés, en synergie avec le maraîchage et toute la biodiversité autour. Marilou sera là pour prêter main forte sur toutes les activités.

<img class="noborder" src="/images/cycle-transp.png"></img>


## Pourquoi participer ?

Pour installer des jeunes prêt·es à participer à la <b>relève</b> du monde agricole ! On veut créer un lieu de <b>rencontres</b> et d'<b>échanges</b>, d’organisation <b>collective</b>, de préservation de la <b>biodiversité</b> et d'expérimentation de l'<b>agroécologie</b>. 

À l'aide de vos dons, nous ferons de cette ferme un bien commun, détenue par une association : nos successeur⋅es n’auront pas à s’endetter pour y travailler. 

[Cliquez ici pour en savoir plus !]( {{< ref "asso" >}} )

## À quoi servira l'argent collecté?

<img class="noborder" src="/images/objectifs.png"></img>

### Palier 1 : 33 000 €

C'est ce qui nous manque pour que l'association puisse acheter la ferme, assurer sa sortie des logiques de spéculation et pérenniser une activité agro-écologique !

### Palier 2 :  + 14 000 € (soit 47 000 € cumulés)

L'association peut acquérir les 3 hectares prévus pour l'activité de maraîchage, permettant de sécuriser le foncier où seront implantées les serres et les buttes de culture !

### Palier 3 : + 9 000 € (soit 56 000 € cumulés)

L'association peut mettre aux normes l'assainissement de la ferme avec un système de phytoépuration !

### Et si on en a vraiment beaucoup..

L'association peut aménager une salle de réunion, une cuisine, une bibliothèque collective et un dortoir pour accueillir visiteureuses et passant·es !

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/les-ami-es-de-la-carette/collectes/achat-d-une-ferme-et-premiers-travaux/widget-bouton" style="width: 100%; height: 70px; border: none;"></iframe>

INFO IMPORTANTE : si vous voulez donner plus de 7000€ et recevoir une attestation fiscale pour que votre don soit défiscalisé  à hauteur de 66%, contactez nous au amies@la-carette.fr !

