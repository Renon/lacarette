---
title: "Points de vente"
date: "2020-01-01"
menu: "main"
description: "Points de vente"
---

Vous pouvez déjà nous retrouver sur les marchés de plein vent suivants :
- à Charavines sur l'avenue du lac (côté lac) le dimanche matin de 7h30 à 12h;
- à St Égrève à Fiancey-Prédieu le vendredi matin de 7h15 à 12h.

Également au magasin de producteurs de la ferme du May à Vourey :
- le vendredi de 16h à 19h;
- le samedi matin de 9h à 12h.

Et pour les puits à pains sans fin, on m'attrape à la sortie du four à Rives les jeudi soirs de 18h à 19h. Me contacter avant de tenter votre chance.

|![alt text](/images/content/pain/paquebot.jpg "La croisière abuse")|
|:--:|
| <p class="caption">Paquebot à trois cheminées en croisière pour vos papilles</p> |

