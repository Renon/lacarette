---
title: "Home"
date: 2024-02-09T14:50:52+01:00
draft: false
---

# Steun de overname van een boerderij door ons collectief!

Ons doel is om een collectieve boerderij op te zetten in Saint-Ondras in het noorden van Isère (Frankrijk). Met z'n zevenen werken we aan een kleinschalig landbouwproject dat geworteld is in de lokale omgeving. Na meer dan drie jaar nadenken, discussiëren, testen en leren, storten we ons eindelijk vol overgave in dit bijzondere avontuur!

|![alt text](/images/neige.jpg "La ferme des Jalinières sous la neige")|
|:--:|
| <p class="caption">De boerderij van Jallinières in de sneeuw</p> |

## Wat is het?

De boerderij die we overnemen is in 2021 gestopt met het produceren van melk: er is dus een hoop werk om alles te veranderen en opnieuw uit te vinden. De aankoop van de boerderij is de eerste stap in het opzetten van verschillende landbouwactiviteiten en een gemeenschapscentrum waar mensen elkaar kunnen ontmoeten en ideeën kunnen uitwisselen (in eerste instantie door middel van participatieve werkdagen, en later met vele andere activiteiten).

|![alt text](/images/croquis_gris.jpg "Croquis de la ferme")|
|:--:|
| <p class="caption">De integratie van de toekomstige ateliers in de huidige boerderij</p> |

## Met wie?

Clément en Florentin hebben in de herfst van 2023 al hun eerste hectare tarwe ingezaaid voor de productie van heerlijk zuurdesembrood! In 2024 starten Emma, Jacob en Nathalie hun tuinderij op de percelen direct tegenover de boerderij. Om Eva's fruitproductie en haar heerlijke confituur te ondersteunen, zullen we hagen en fruitbomen planten in symbiose met de tuinbouw en alle biodiversiteit eromheen. Marilou steekt de handen uit de mouwen om te helpen bij alle activiteiten op en rond de boerderij.

<img class="noborder" src="/images/cycle-transp.png"></img>


## Waarom bijdragen?

Om jonge mensen te ondersteunen die klaar zijn om de volgende generatie boeren te vervoegen! We willen een plek creëren voor ontmoetingen en uitwisselingen, collectieve organisatie, behoud van de biodiversiteit en het experimenteren met agro-ecologische principes en praktijken.

Met behulp van jullie donaties maken we van deze boerderij een gemeenschappelijk bezit (commons), eigendom van een vereniging: onze opvolgers zullen zich niet in de schulden moeten werken om deel te kunnen uitmaken van het project.

[Klik hier voor meer informatie !]( {{< ref "asso.nl" >}} )

## Kostenbeschrijving: waar wordt het ingezamelde geld voor gebruikt?

<img class="noborder" src="/images/objectifs.png"></img>

### Stap 1 : € 33 000

Dit is wat we nodig hebben om de vereniging in staat te stellen de boerderij te kopen, om ervoor te zorgen dat deze voortaan aan de speculatie-logica ontsnapt en we er een agro-ecologische activiteit voor kunnen bestendigen!

### Stap 2 :  + € 14 000 (in totaal € 47.000)

De vereniging kan de 3 hectare aankopen die gepland zijn voor de groenteteeltactiviteit, en de grond veiligstellen waarop de serres en de kweekheuvels zullen worden gebouwd!

### Stap 3 : + € 9 000 (in totaal € 56.000)

De vereniging kan voldoen aan de wettelijke normen voor sanitaire en watervoorzieningen van de boerderij plantaardig zuiveringssysteem te installeren!

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/les-ami-es-de-la-carette/collectes/achat-d-une-ferme-et-premiers-travaux/widget-bouton" style="width: 100%; height: 70px; border: none;"></iframe>

**Belangrijk** : *als je meer dan €7.000 wilt doneren en een belastingcertificaat wilt ontvangen zodat je donatie fiscaal aftrekbaar is, neem dan contact met ons op via amies@la-carette.fr.*

