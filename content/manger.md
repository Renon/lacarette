---
title: "Manger"
date: "2020-01-01"
menu: "main"
description: "Catalogue des produits"
---

## Le pain au levain

Tous les pains sont fabriqués avec de la farine de meule bio, au pur levain et cuits dans un four chauffé au bois. Ils sont également pétris à la main.

Tout ça pour produire une miche :
- **savoureuse** : acidulée au levain, aux arômes de noisette et caramel par la cuisson;
- **nourrissante** : une tartine généreuse qui éloigne les fringales et aussi les petits soucis (impact réduit sur le taux de sucre = index glycémique faible);
- **digeste** : le levain dompte les gluten, et avec des farine peu raffinées, on prend soin de nos intestins;
- **fidèle** : elle ne vous laissera pas tomber en se desséchant dans la journée, mais restera tendre toute la semaine dans son torchon et sa huche !

|![alt text](/images/content/pain/dessous.jpg "Photo satellite")|
|:--:|
| <p class="caption">Photo satellite sous la miche, méandres caramels et canyons cafés</p> |

## Catalogue

![alt text list left](/images/content/pain/campagne.jpg "Pain de campagne") <p class="prix">**Pain de campagne** *(levain de seigle, farine bise de blé, sel de Batz-Guérande)* <br><br> Miche 800g : 4€ <br> À la coupe (moitié) : 5€/kg <br></p>

<br>

![alt text list right](/images/content/pain/complet.jpg "Pain complet") <p class="prix">**Pain complet** *(levain de seigle, farine bise de blé, son très fin, sel de Batz-Guérande)* <br><br> Miche 800g : 5€ <br> À la coupe (moitié) : 6.25€/kg <br></p>

<br>

![alt text list left](/images/content/pain/tournelin.jpg "Pain tournelin") <p class="prix">**Pain "Tournelin"** *(levain de seigle, farine bise de blé, graines de tournesol, graines de lin brun, sel de Batz-Guérande)* <br> Miche 800g : 6€ <br> À la coupe (moitié) : 7.5€/kg <br></p>

<br>

![alt text list right](/images/content/pain/meteil.jpg "Pain de méteil") <p class="prix">**Pain de méteil aux noix** *(levain de seigle, farine bise de blé, noix de Vourey, farine de seigle T130, sel de Batz-Guérande)* <br> Miche 800g : 6,5€ <br> À la coupe (moitié) : 8.13€/kg <br></p>
<!--
![alt text list right](/images/content/pain/meteil.jpg "Pain de méteil aux noix") <p class="prix">**Pain de méteil aux noix** *(levain de seigle, farine bise de blé, farine de seigle T130, noix de Grenoble, sel de Batz-Guérande)* <br> Miche 800g : 6.5€ <br> À la coupe (moitié) : 8.13€/kg <br></p>
-->

<br>

![alt text list left](/images/content/pain/engrain.jpg "Pain d'engrain") <p class="prix">**Pain de petit épeautre** *(levain de petit épeautre, farine de petit épeautre intégrale et bise, sel de Batz-Guérande)* <br> Moulé 800g : 7.5€ <br> À la coupe (moitié) : 9.4€/kg <br></p>

<br>

## La brioche pur beurre au levain

![alt text list left](/images/content/brioche/brioche.jpg "Brioche au levain") <p class="prix">**Brioche au levain** *(farine de blé, oeufs, beurre, levain de seigle, sucre, levure, sel de Batz-Guérande)* <br> Entière (800g) : 12€ <br> Moitié (400g) : 6€ <br> Quart (200g) : 3€ <br> </p>

<br>

## Des biscuits croquants et gourmands

![alt text list left](/images/content/biscuit/amande.jpg "Amande") <p class="prix">**Amande** *(farine bise de blé, sucre, beurre, poudre d'amande, oeufs, poudre à lever, sel de Batz-Guérande)* <br><br> Par 100g : 2€ <br>  <br></p>

<br>

![alt text list right](/images/content/biscuit/CG.jpg "Citron Gingembre") <p class="prix">**Citron Gingembre** *(farine bise de blé, sucre, beurre, oeufs, jus de citron et zeste, poudre de gingembre, poudre à lever, sel de Batz-Guérande)* <br><br> Par 100g : 2€ <br>  <br></p>

<br>

![alt text list left](/images/content/biscuit/choco.jpg "Choco") <p class="prix">**Choco** *(farine bise de blé, sucre, beurre, oeufs, pépites de chocolat au lait, palets de chocolat noir, poudre à lever, sel de Batz-Guérande)* <br><br> Par 100g : 2€ <br>  <br></p>

<br>

![alt text list right](/images/content/biscuit/sara.jpg "Sarrachoc") <p class="prix">**Sarrasin Choco** *(farine bise de sarrasin, sucre, beurre, oeufs, pépites de chocolat noir, palets de chocolat noir, poudre à lever, sel de Batz-Guérande)* <br><br> Par 100g : 2€ <br>  <br></p>

<br>
