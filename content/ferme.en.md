---
title: "The farm"
date: 2024-02-09T14:50:52+01:00
draft: false
---

The **Jallinières** farm has ceased its dairy farm activities in 2021: all farming activities thus need to be transformed and reinvented.

The farm currently comprises the following buildings:
* a 150 m²  house, where we will initially live, but which we will gradually transform into a collective space (with meeting room, shared library, communal kitchen, dormitory to accommodate visitors);
* a 110 m² adobe barn for grain storage, sorting and milling;
* a barn, milking parlour and dairy room, whose 300 m2 will be completely refurbished and shared between the bakery and the washing, cleaning and storing of the vegetables;
* a 1,000 m² area of land adjoining the existing buildings, where a new building will eventually be erected.

In addition to the buildings, the core of every agricultural project of course remains the arable land. The farm comprises around 47 hectares, of which 15 hectares are owned by the current farmer and 32 hectares are leased. The farmer will gradually sell his land, and we are currently making sure that the other leases will be renewed to allow us to consolidate our project.
