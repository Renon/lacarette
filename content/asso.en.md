---
title: The association
date: 2023-01-01
menu: main
description: Présentation des associations
draft: false
---

We want to convert the farm into a common good: this way our successors will not have to take on huge loans to be able to work on the land. In this, we aim to break the vicious circle of debt.

Inspired by other peasant collectives who have tested this model before us, our legal structure is based on an associative model. Since no one can claim individual ownership over the farm and the land surrounding it, and the association’s statutes specify that the farm pursues an agro-ecological mission and social justice, we protect the farm both collectively as well as in the long term! To this end, we have set up the association "Les ami-es de la carette", which has the following objectives:

1. To perpetuate peasant farming by liberating agricultural land from real-estate speculation and the market economy. We want agricultural land and buildings to retain their agricultural vocation and contribute to the region's food self-sufficiency, without creating debt for new farmers.
2. To create a space where food is produced, transformed and sold in an environmentally-friendly way. We have chosen to participate in the development of peasant and organic farming. Our aim is to enable farmers to make a living from their work, while simultaneously reinforcing biodiversity through agro-ecological practices.
3. To value and practice collective, anti-authoritarian modes of organization. We want to put into practice forms of living and working that are collective, solidary, self-managed and mindful of mechanisms of systemic oppression.

<img class="noborder" src="/images/montage.png"></img>

To separate ownership from use and to consolidate the farm beyond our own project, there will be two associations on the farm: one that owns the farms and another that manages its activities. These two associations are complemented by the economic structures (agricultural production).

* The ‘owner’ association "Les ami-es de la carette" brings together contributors who support the project with their capital. It will buy the buildings and lease them to the management association.
* The ‘management’ association "Les passager-es de la carette" will be responsible for managing the site, taking charge of any renovations to be carried out, and leasing the buildings to agricultural structures such as GAECs (groupements agricoles d'exploitation en commun), a type of collective farming company.
* The two farming companies (cereals and vegetables) and other structures that could potentially be set up in the future will have use of the buildings and land. The rent these companies pay to the association will progressively reimburse the contributions and loans made to the owner association.
