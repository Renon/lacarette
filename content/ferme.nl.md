---
title: "De boerderij"
date: 2024-02-09T14:50:52+01:00
draft: false
---

De boerderij bestaat momenteel uit verschillende gebouwen:
- een huis van 150 m2, waar we aanvankelijk zullen wonen maar dat we geleidelijk zullen omvormen tot een collectieve ruimte met vergaderzaal, gedeelde bibliotheek, gemeenschappelijke keuken, en een slaapzaal voor bezoekers;
- een lemen schuur van 110 m2 die gebruikt zal worden voor het opslaan en sorteren van graan en voor het malen;
- een schuur, melkstal en zuivellokaal, waarvan 300m2 volledig gerenoveerd zal worden en gebruikt zal worden voor de opslag, het wassen en verpakken van groenten, en de bakkerij;
- een stuk grond van 1000 m² grenzend aan de bestaande gebouwen, waar uiteindelijk een nieuw gebouw zal verrijzen.

Naast de gebouwen is het hart van elke boerderij de landbouwgrond. De boerderij heeft ongeveer 47 hectare land, waarvan 15 hectare eigendom is van de verkoper en 32 hectare gepacht wordt. Dankzij deze crowdfunding-campagne kan de vereniging de 3 hectare aankopen die bestemd zijn voor de tuinbouw. Het doel op de lange termijn is om geleidelijk al het land op te kopen via de vereniging om zekerheid te bieden aan toekomstige boeren.

## Geschiedenis: overname en verkoop

De huidige boer stopte in 2021 met de productie van biologische melkkoeien op de boerderij. Sindsdien wilde hij niet dat zijn boerderij zou bijdragen aan de uitbreiding van andere boerderijen, en onze wegen kruisten in januari 2023. Sindsdien werken we samen om ervoor te zorgen dat de overname van deze boerderij een succes wordt voor beide partijen, zodat de jarenlange ervaring van de boer gevaloriseerd wordt en de boerderij kan evolueren om aan onze projecten tegemoet te komen. Het hele jaar door hebben we samengewerkt en ondertussen is het project verfijnd dankzij de steun van veel mensen op juridisch, fiscaal, boekhoudkundig, menselijk en landbouwkundig gebied.
