---
title: La ferme
date: 2024-02-09T14:50:52+01:00
draft: false
---
La ferme des <b>Jallinières</b> a arrêté son activité de vaches laitières en 2021 : tout est à transformer et à réinventer. Maintenant c'est la ferme du Combaud !

La ferme se compose actuellement de plusieurs bâtiments :

* une maison d'habitation de 150 m², où nous habiterons au début mais que nous transformerons petit à petit en espace collectif (salle de réunion, bibliothèque partagée, cuisine commune, dortoir pour héberger des visiteur.euse.s);
* une grange en pisé de 110 m² qui accueillera le stockage et le tri des céréales ainsi que la meunerie ;
* une stabulation, une salle de traite et une laiterie, dont les 300m2 seront complètement réaménagés et partagés entre le stockage, lavage et conditionnement des légumes et la boulangerie ;
* un espace terrassé de 1000 m² attenant aux bâtiments existants, où a terme un nouveau bâtiment verra le jour.

En plus des bâtiments, le cœur de chaque installation agricole reste la surface cultivable. La ferme compte environ 47 hectares, dont 15 hectares en propriété du cédant et 32 hectares en location (fermage). Le cédant vendra ses terres progressivement ; nous nous assurons en ce moment que les autres baux seront reconduits pour nous permettre de nous installer.