---
title: "Home"
date: 2024-02-09T14:50:52+01:00
draft: false
---

# Support the purchase of a farm by our collective!

We want to build a thriving farming collective at the Jallinières farm in **Saint-Ondras**, in northern Isère (France). The seven of us are working together on a project for **small-scale organic farming** rooted in the local ecosystem and community. After more than three years of reflection, (de)construction, learning and unlearning, our collective is now ready to really launch this extraordinary adventure!

|![alt text](/images/neige.jpg "La ferme des Jalinières sous la neige")|
|:--:|
| <p class="caption">The Jallinières farm in the snow</p> |

## What’s the project about?

The Jallinières farm has ceased its dairy farm activities in 2021: all farming activities thus need to be transformed and reinvented. The purchase of the farm is the first step towards the creation of several agricultural activities and an associative space for meetings and exchange (organised around collective workdays in the beginning, and many other things in the long term).

|![alt text](/images/croquis_gris.jpg "Croquis de la ferme")|
|:--:|
| <p class="caption">The future activities on the farm</p> |

## Who’s involved?

The first activity to be launched is the production of **wheat** along with a **flour mill** and a **bakery**: Clément and Florentin have already sown their first parcels in the autumn of 2023! In 2024, Emma, Jacob and Nathalie will launch the **horticulture** (vegetables) activity on the plots of land just in front of the farm. Then, lots of **hedges and fruit trees** will be planted for Eva's fruit production activity and delicious jams, in synergy with the vegetable production and the surrounding biodiversity. Marilou will be there to lend a hand with all the different activities.

<img class="noborder" src="/images/cycle-transp.png"></img>


## Why contribute?

To help young people who are ready to take part in the next generation of farmers to kickstart their activities! We want to create a place where people can meet and exchange ideas and practices, organize collectively, preserve biodiversity and experiment with agroecology.

With your donations, we will turn this farm into a common good, owned by an association: this way our successors will not have to take on huge loans to be able to work on the land.

[Click here to find out more about the association!]( {{< ref "asso" >}} )

## What will the money raised be used for?

<img class="noborder" src="/images/objectifs.png"></img>

### Stage 1: € 33,000

This is the amount we still need to buy the farm, to ensure that it is no longer subject to market speculation, and to establish its agro-ecological orientation!

### Stage 2: + € 14,000 (i.e. a total of € 47,000)

At this stage, the association is able to acquire the 3 hectares of land for horticulture, effectively securing the land on which the greenhouses and permanent planting beds will arise!

### Stage 3: + € 9,000 (i.e. a total of € 56,000)

The association is able to upgrade the farm's sewage system up to legal standards by means of a plant-based filter system!

### And if we really receive a lot of donations...

The association is able to set up a meeting room, a kitchen, a collective library and a dormitory to welcome visitors and passers-by!

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/les-ami-es-de-la-carette/collectes/achat-d-une-ferme-et-premiers-travaux/widget-bouton" style="width: 100%; height: 70px; border: none;"></iframe>

IMPORTANT: if you would like to donate more than € 7,000 and receive a tax certificate so that your donation can be tax-deducible, please contact us at amies@la-carette.fr!

