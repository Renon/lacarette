---
title: "De vereniging"
date: 2023-01-01
menu: main
description: "Présentation des associations"
draft: false
---

Met behulp van jullie donaties maken we van deze boerderij een gemeenschappelijk bezit (commons), eigendom van een vereniging: onze opvolgers zullen zich niet in de schulden moeten werken om deel te kunnen uitmaken van het project. Geïnspireerd door andere boerencollectieven die dit model voor ons hebben uitgeprobeerd, is onze juridische structuur gebaseerd op een associatief model. Aangezien niemand individueel eigendom kan claimen van deze boerderij en het land eromheen, en er in de statuten van de vereniging staat dat deze boerderij een agro-ecologische missie en sociale rechtvaardigheid nastreeft, wordt het zowel collectief als op lange termijn beschermd. Om deze missie uit te werken hebben we de vereniging “Les ami-es de la Carette” opgericht, die de volgende doelstellingen heeft:

1. **De toekomst van de kleinschalige landbouw veiligstellen door landbouwgrond te beschermen tegen speculatie en de markteconomie.**
We willen dat landbouwgrond en boerderijgebouwen hun agrarische bestemming behouden, zodat ze kunnen bijdragen aan regionale voedselzelfvoorziening, zonder schulden te creëren voor nieuwe gebruikers.
2. **Een milieuvriendelijk landbouwmodel creëren.**
We hebben ervoor gekozen om deel te nemen aan de ontwikkeling van kleinschalige, biologische landbouw. Het doel is om boeren in staat te stellen van hun werk te leven en tegelijkertijd de biodiversiteit te verbeteren door middel van agro-ecologische praktijken.
3. **Collectieve, anti-autoritaire organisatievormen bevorderen.**
We willen vormen van leven en samenwerken in praktijk brengen die collectief, solidair en zelfgestuurd zijn en die bijdragen tot het ontmantelen van structurele oppressie.

<img class="noborder" src="/images/montage.png"></img>

## Juridische structuur: een eigendomsvereniging en een beheersvereniging

Om het eigendom en het gebruik van de boerderij te scheiden en ervoor te zorgen dat de boerderij ook na onze persoonlijke projecten blijft bestaan, zullen er twee verenigingen op de boerderij aanwezig zijn: een vereniging die eigenaar is van de boerderij en het omringende land, en een vereniging die het beheer van de boerderij op zich neemt. Bovenop deze verenigingen komen de landbouwbedrijven die instaan voor de landbouwproductie.
- De eigendomsvereniging “Les ami ⋅ es de la Carette”, bestaat uit donateurs die het project steunen met hun kapitaal. Deze vereniging zal de gebouwen kopen (aan het einde van deze inzamelingsactie) en ze verhuren aan de beheersvereniging op basis van een erfpachtcontract.
- De beheersvereniging “Les passager ⋅ ères de la Carette” zal verantwoordelijk zijn voor het beheer van de boerderij, de uit te voeren werkzaamheden op zich nemen en de gebouwen verhuren op basis van erfpacht aan landbouwstructuren *(GAEC; groupement agricole d'exploitation en commun)*.
- De twee landbouwbedrijven GAEC (graanteelt en tuinbouw) en toekomstige structuren die ter plekke werkzaam zijn, zullen gebruik kunnen maken van de gebouwen en de grond. Hun huur wordt gebruikt om de bijdragen en leningen aan de eigendomsvereniging terug te betalen. 
