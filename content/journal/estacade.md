---
title: "Estacade et brioches"
date: 2022-07-30T16:01:08+02:00
draft: false
meta_img: "/images/content/brioche/brioche.jpg"
---

Dans la morne plaine des vacances estivales, on ne s'arrête pas de buller : nouveau marché et nouvelle beurrée.

#### Estoc et taille sur l'Estacade

La danse des couteaux à pain résonne désormais sur la partie la plus septentrionale du marché de l'Estacade. À partir de ce début d'août, des miches seront déballées tous les mercredi sur ce marché, sous l'étendard du Levain Bullant (banderole en préparation, porte-drapeau en formation).

Ceci pour assurer la continuité de Léa, collègue boulangère en émigration, et proposer du pain dans le creux de la semaine. Un public sans doute différent des samedi et dimanche, plus habitué et ouvert aux échanges.

#### La conquête de la mie filante

Pour profiter un max des chaleurs inhumaines de ce mois d'août (mais toujours un des plus frais du reste de nos heures), une espèce de pâte à l'oeuf massée au beurre ronfle tranquillement dans une lente inspiration. Elle expirera pendant 45 minutes à 190°C pour dévoiler ses quatre boules rebondies et dorées, l'enfilade filant l'eau à la bouche.

|![alt text](/images/content/brioche/brioche.jpg "L'enfilade filante")|
|:--:|
| <p class="caption">La mie filante en pose</p> |


