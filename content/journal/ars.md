---
title: "Au fournil chez Robert"
date: 2021-09-06T18:08:11+01:00
draft: false
meta_img: "/images/content/ars/feu.jpg"
---

#### Les trois fours d'Ars

Dans le hameau d'Ars, aux environs de Pérols-sur-Vézère sur le plateau de Millevaches, on compte une dizaine d'habitants pour trois four à pain, à moins de cent pas les uns des autres. Le premier est au centre du lieu : un four à pain en pierre qui a perdu son chapeau, dont l'intérieur voit pousser les herbes folles des puits de lumière par sa voûte. Le second est à l'abri d'une chaumière, ou plutôt la chaumière intégrée au four : les proportions de ce four en énormes blocs de granite impose une préchauffe d'une semaine. Le dernier est celui de Robert, résident paisible et généreux, qui nous a invité dans son fournil de pierre à enflammer le four romain et sa voûte en briques.

|![alt text](/images/content/ars/feu.jpg "Johnny allume ce four !")|
|:--:|
| <p class="caption">Les flammes ravivent tranquillement la fonction première du four</p> |

#### Chauffe périlleuse

N'ayant aucune idée de la masse de briques et de pierres à chauffer sur les côtés et le dessus du four, on s'y prend 3 jours en avance avec un dérhumage progressif (chauffe douce pour sécher les pierres). Ça nous laisse le temps de partir en forêt avec Robert et son quad pour refaire son stock de charbonnettes en prévision du coup de feu.

Le jour du pain, le feu est baladé dans tous les recoins du four, et la flamme constamment nourrie. Tant que les braises ne sont pas sorties, impossible d'avoir une idée exacte de la fournaise qui règne là-dedans. La voûte devient blanche.

Lorsque l'heure du pain arrive, les braises sont réparties sur la sole pour rendre une ultime chaleur à la sole et à la voûte.

|![alt text](/images/content/ars/braises.jpg "Lumière éteinte, une voie lactée de braises")|
|:--:|
| <p class="caption">Le lit de braises rougeoyantes assure une chaleur homogène</p> |

#### L'élevage de la pâte

Le travail du boulanger a commencé trois jours avant, pour donner à son levain les trois rafraichis avant d'ensemencer la pâte au pétrissage. Les pâtons sont divisés, boulés et façonnés sous plusieurs formes (tabatières, auvergnats, bâtards).

|![alt text](/images/content/ars/tabac.jpg "Pâtonnage à Ars")|
|:--:|
| <p class="caption">Une tabatière s'apprête à pousser sur ce chiffon</p> |

Commence alors une course entre la fermentation des pâtons façonnés et la chauffe du four : four froid et pâtons plats, ou four trop chaud et pâtons en sceaux ? Après évacuation des braises, on envoie un coup de panosse (balai-serpillère) pour enlever les dernières cendres et on test par jet de farine la température de la sole. Son roussissement indique si le four est prêt à recevoir le pain.

#### L'enfournement

Les premiers mets à visiter la fournaise sont les pizzas, qui passeront moins de 3 minutes dans un four aux alentours de 350°C.

|![alt text](/images/content/ars/pizzas.jpg "Roulette à pizzas et cartons plats")|
|:--:|
| <p class="caption">Sauce tomate maison, et croûtes bien saisies par le feu</p> |

Les pains succèdent aux pizzas et ressortent 40 minutes plus tard, juste à temps : le four est bouché pendant la cuisson, ce qui empêche tout contrôle visuel ! Bonne surprise au final, on a évité le tas de charbon et les mangereuses repartiront avec leur miche.

|![alt text](/images/content/ars/défour.jpg "Défournement à la frontale d'antan")|
|:--:|
| <p class="caption">Des belles livres de pain en ressuage sur la charbonnette locale</p> |
