---
title: "Du sel de Batz-Guérande"
date: 2022-01-07T15:56:02+01:00
draft: false
meta_img: "/images/content/sel/mulon.jpg"
---


|![alt text](/images/content/sel/mulon.jpg "Sel de Batz")|
|:--:|
| <p class="caption">Mulon de sel - Crédits: sel2batz.com</p> |

En ce début d'année 2022, le vent bullant se pose sur la presqu'île de Guérande pour un ravitaillement en sel : une petite virée à Batz-sur-Mer à la rencontre de Pascal et Delphine du GAEC de la Salorge Rouge.

Ces paludiers récoltants travaillent le sel depuis 20 ans maintenant, avec un passage à la mention Nature et Progrès entretemps (il n'existe pas de label AB pour le sel). Sur les 3ha de marais en activité, la période propice à la récolte peut se concentrer en un petit mois l'été, si les vents d'Est bien secs soufflent dans les œillets. Des journées longues comme un jour sans pain !

|![alt text](/images/content/sel/fares.jpg "Sel de Batz")|
|:--:|
| <p class="caption">Les œillets ou cristallisoirs - Crédits: sel2batz.com</p> |

De mon côté je repars avec 60kg de gros sel (une petite année de pain) et de la fleur de sel pour la bonne cuisine.

La vente directe est ouverte presque toute l'année, n'hésitez pas à leur rendre visite, ils vous expliqueront tout. À cette adresse :

```text
GAEC LA SALORGE ROUGE
Route du Marais
(C’est le dernier Bâtiment sur la route du Marais en sortant de Kervalet)
KERVALET
44740 BATZ-SUR-MER
```

On peut même les retrouver sur la toile : [sel2batz.com](http://www.sel2guerande.com/)
