---
title: Débuts de la ferme du Combaud !
meta_img: /images/tracteur-remorque.jpg
date: 2024-04-11T18:34:08.747Z
draft: false
---
Si l’on tend bien l’oreille, on peut entendre au coeur des Terres Froides la masse qui bat le parpaing dans la ferme fraîchement renommée : La ferme du Combaud ! Aucun doute, les chantiers sont en route, et ils débutent en grandes pompes.

#### Planification des travaux

Nous avons décidé d’échelonner dans le temps le lancement des différents ateliers, pour concentrer nos énergies dans les travaux et permettre le lancement d’une activité économique au plus vite. Dans les semaines et mois à venir, nous nous concentrons donc sur la construction des espaces dédiés à la meunerie et au fournil, respectivement situés dans l’ancienne grange et la zone laiterie – salle de traite - stabulation. Cette dernière a vocation a être divisée en deux, pour accueillir dans un deuxième temps les espaces de stockage et lavage des légumes.

#### La grange

Ici, la grange, refuge historique des hirondelles, se vide de son foin pour bientôt laisser place à un nouveau plancher. C’est avec méthode que les apprentis funambules remplacent les filards, fines branchettes de bois qui traditionnellement servaient de plancher pour le foin et la paille qui séchaient au dessus des vaches, par de robustes planches qui nous permettront de stocker divers outils en attendant l’avancement des travaux au sol.

![une personne s’apprête à sauter dans une remorque de foin](/images/1.jpg "Florentin pret à sauter dans cette grande aventure !")

![une personne pose sur un plancher de grange](/images/2.jpg "Emma à l'oeuvre sur le plancher de la grange")

### Tractofolie

Pendant que certain·es s’affairent au foin, d’autres s’affairent au fumier. C’est aussi l’occasion pour tout le monde de sympathiser avec les différents tracteurs de la ferme, remettre en route ceux qui dormaient depuis longtemps, et démarrer les allées et venues avec la remorque. Un grand merci à Antoine, qui est venu nous aider à réparer la masse d’un des tracteurs.

![un tracteur remorque un autre en marche arrière](/images/tracteurs.jpg "Un tracteur qui en tracte un autre...")

![Chantier finesse : réparation d'une masse de tracteur](/images/antoine-masse.jpg "Chantier finesse : réparation d'une masse de tracteur")

### La salle de traite

L’ensemble de l’infrastructure composant la salle de traite a été démontée pour pouvoir être remontée sur une autre ferme. Bientôt, des images des murs qui tombent, des niveaux de sols qui s’ajustent pour laisser place à une dalle toute fraîche !

![une salle de traite en train d’être démontée](/images/salle-de-traite.jpg "De la salle de traite au fournil, beaucoup d'étapes à venir !")

![une personne en train de disquer du métal](/images/disqueuse-.jpg "Ici, ça disque fort !")

### Entre deux coups de masse, du remplissage de tableur

Un peu moins visible et plus difficile à mettre en image, le parcours d’installation est aussi synonyme de nombreuses démarches administratives et d'acronymes : demande de la DJA (dotation jeune agriculteur·ice), et montage du premier GAEC (groupement agricole d’exploitation en commun) pour Clément et Florentin : le GAEC du Soufflage !

Pour la petite histoire, le soufflage c’est un mouvement dans la préparation du pain qui consiste à emprisonner l’air dans la pâte pour lui donner de la force.

### Partout sur la ferme, on s’affaire...

En parallèle de ces chantiers, plusieurs autres se mettent en place : grand nettoyage de la maison collective, analyses de l’eau de la source qui arrive sur la ferme, analyses de sol sur différentes parcelles. Un grand merci à Bernard, pour son aide judicieuse sur ce dernier point et pour son agilité dans la taille des pommiers et noyers du verger !

### ...sans oublier de lever la tête, pour profiter de la vue !

On vous laisse sur cette belle prise, et on revient vite avec des nouvelles des chantiers :)

Ah oui ! Avec les beaux jours qui arrivent, on va pas tarder à organiser des chantiers collectifs, et si ça vous intéresse d’être tenu.es informé.es, hésitez pas à nous écrire à **fermeducombaud@la-carette.fr**

L’équipe du Combaud

![Un très beau coucher de soleil ](/images/coucher-soleil.jpg)