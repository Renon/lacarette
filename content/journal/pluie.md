---
title: "Marché pluvieux, copains heureux"
date: 2022-05-06T16:01:08+02:00
draft: false
meta_img: "/images/content/pluie/etal.jpg"
---

Après quelques mois la tête sous l'eau, on se sèche près du feu retrouvé : la remorque a trouvé son atelier (merci Roger), et les premiers pains sont fabriqués. Trois fournées et deux semaines de tests, d'expérimentations plus ou moins heureuses et d'écoulement des surplus au voisinage, avant le premier marché ce jeudi, et la production la veille ce mercredi !

|![alt text](/images/content/pluie/indent.jpg "Faut pas pousser les pâtons")|
|:--:|
| <p class="caption">Test de réponse du pâton à une indentation digitale bien dosée</p> |

C'est donc une première journée de véritable travail de boulanger, après avoir navigué entre chaudronnerie, menuiserie et plomberie. Seuls les pains de petit épeautre, débordant d'une fermentation généreuse, ont marqué une journée bien réglée avec trois pétrissées et trois fours consécutifs. Pétrir à la main et dans un four assez petit (20kg de pain) exige de s'y prendre à plusieurs fois, et de ne pas confondre les pâtes : un pâton qui manque son four et prend celui d'après ressortira sous forme de galette (testé et désapprouvé).

Quelques brûlures plus tard, les trois fournées sont sorties, ressuées sur grille, puis mises en caisse et embarquées avec leur matériel de scène pour la tournée du lendemain. Une courte nuit précède le réveil pluvieux du jeudi, soulagement des agriculteurs mais hantise du marchand ambulant. Direction Rives, calage entre le poissonnier et la fromagère et déballage des miches et des sablés.

|![alt text](/images/content/pluie/etal.jpg "Le sourire du pari perdu")|
|:--:|
| <p class="caption">Du rouge rosé pamplemousse pour accompagner ces jolis brichetons</p> |

Ambiance morose et pas grisante de grisaille ce matin là, mais quelques hardis tordent leurs habitudes et viennent s'enticher d'un pain de campagne ou d'un méteil noix avec curiosité. Une écolière gourmande convaincue par un échantillon de sablé revient dix minutes plus tard avec quelques sous pour en prendre "cinq de plus".

Un marché bien trop paisible, pas de quoi "travailler" ni "croûter" pour l'artisan, mais toujours une expérimentation avec indemnisation partielle des frais de vestiaires, de farine et de combustible. Les heureux de l'averse sont après le marché, les copains (c'est-à-dire les collègues de miche), qui sauvent les pains du rassissement.
