---
title: "La lutte des bulles"
date: 2023-06-02T14:33:51+01:00
draft: false
meta_img: "/images/content/printemps/semis_couchant.jpg"
---

Le levain a bullé sans cesser à travers l'hiver. Les températures négatives n'ont pas réussi à empêcher les miches de sourdre du four. Pas plus qu'elles n'empêchent le boulanger positif à sortir du lit.

#### Va-et-vient

Après quelques oscillations des températures, l'été est là, l'hiver est défait. Dans mon fournil à trois murs, ces variations imposent de jongler avec le levain, l'eau chaude dans la pâte et les durées pour que fermentations et température du four se croisent à point. Un jour les pâtes galopent et le four pulse de feu pour tenir le rythme. Un autre les pâtons tardent à se réveiller, et le four s'assoupit sur ses braises. 

|![alt text](/images/content/printemps/vapeur.jpg "Couille Hammam")|
|:--:|
| <p class="caption">Locomotive à pleine vapeur pressée par l'horloge de la fermentation</p> |

Prise de position : un pain plat mais fermenté sera toujours plus tendre et savoureux qu'un pain trop "jeune", qui explose au four, au goût de farine crue et à la mâche pâteuse. Cependant, la loi du marché (de l'Estacade) délaisse les premiers pour les seconds, tout en spectacle et en fausses promesses. Le juste point de fermentation est donc aussi important qu'une mise au point sur l'étal.

#### Semer aussi

La terre se réchauffe, c'est aussi le moment de lui confier quelques graines. Avec le collègue Clément, nous avons semé la semaine passée un hectare de tournesols, après un premier semis offert aux limaces. Manoeuvre de s'enraciner par procuration, et de laisser sa confiance aux mottes grumeleuses pour faire leur oeuvre.

|![alt text](/images/content/printemps/semis_couchant.jpg "Loco tourniquet")|
|:--:|
| <p class="caption">Chemin tracé et bonne levée, puis bullant dans le couchant</p> |

La terre sort de l'hiver, nous aussi, en y laissant parfois un ami. C'est le moment de réchauffer nos liens, d'en construire de nouveaux et de fêter cette belle pelote d'élastiques. Alors retrouvons nous **vendredi 16 juin** pour une soirée avec des pizzas sorties du four à pain, vos boissons préférées, vos amis et votre voisin préféré. Deuxième édition et version printanière de la fête du Bois Vert !

|![alt text](/images/content/printemps/daubé_cadré.jpg "Daubé cadré")|
|:--:|
| <p class="caption">Le sympathique comité informel et le particulier à besoin professionnel</p> |
