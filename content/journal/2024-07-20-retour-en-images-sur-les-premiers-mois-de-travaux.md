---
title: Retour en images sur les premiers mois de travaux
meta_img: /images/pelles.jpg
date: 2024-07-20T18:07:48.049Z
draft: false
---
# Première étape achevée !

Après les premiers temps d’installation et de rangement de la ferme, nous avons réalisé le gros œuvre des travaux de la meunerie et du fournil. On vous fait un point d’étape en images !

## Avril : démolition des anciennes laiterie et salle de traite 

Après avoir démonté le toit de la salle de traite et de la laiterie et fait tomber quelques murs au tracteur, nous avons appelé Yves en renfort pour le gros de la démolition et du terrassement. Avec ses pelles, il nous a aidé à décaisser le sol de la grange, et à démolir les murs et l’ancienne dalle. Puis le terrassement a commencé pour pouvoir couler la nouvelle dalle. Ainsi tous les bâtiments seront au même niveau entre la grange qui accueillera la meunerie et l’ancienne laiterie et salle de traite qui accueilleront le fournil. 

|![alt text](/images/pelles.jpg "Chantier de démolition avec un travailleur en premier plan")|
|:--:|
| <p class="caption">Démolition de l’ancienne salle de traite</p> |

En parallèle, d’autres chantiers continuent dans l’atelier, qui a été entièrement vidé et trié. Nous avons sorti une cuve a fioul a été sortie et raccordé la maison à l’eau de ville. Dans la grange, nous avons piqué les murs pour débarrasser le pisé de l’enduit en ciment. 

|![alt text](/images/tuyau.jpg "Personne accroupie de dos faisant de la plomberie")|
|:--:|
| <p class="caption">Raccordement de la maison à l’eau de ville</p> |

|![alt text](/images/rangement.jpg "Personne triant et rangeant des outils")|
|:--:|
| <p class="caption">Tri des outils et rangement de l’atelier</p> |

En avril nous avons également eu le plaisir d’accueillir nos copaines de Montpellier pour un premier évènement public : un atelier de mécanique auto en mixité choisie avec les déculassé∙e∙s qui étaient en tournée ! Initiation à la mécanique et vidange, le soleil était au rendez-vous pour que les participantes puissent mettre les mains dans le cambouis. 

|![alt text](/images/ciment.jpg "Personne sur un escabeau piquant l'enduit en ciment sur un mur en terre")|
|:--:|
| <p class="caption">Piquage de l’enduit en ciment sur la grange en pisé</p> |

## Mai : préparation de la dalle

Cette dalle nous a bien occupé∙es tout le mois de mai ! Montage d’un muret, préparation du coffrage, mise à niveau avec des graviers, autant d’étapes préliminaires qui nous ont fait manier pioches, râteaux et truelles.

Il a d’abord fallu creuser les fondations du muret qui sépare les deux niveaux de dalle. Ces deux niveaux sont conçus pour faciliter le travail futur que ce soit pour le chargement du pain dans les véhicules de livraison ou pour alimenter le four en bois. Puis la bétonnière a bien tourné pour remplir les fondations et les blocs à bancher. Une équipe a ensuite ratissé des graviers sans relâche sous l’œil intransigeant du niveau laser pour remplir l’espace de la dalle de graviers. Pendant ce temps, les autres réalisaient les coffrages. Dernier rush pour l’étape finale de la pose de l’isolant sous l’emplacement du four la veille de l’arrivée des toupies de béton !

|![alt text](/images/triptyque.png "Montage de trois photos montrant les étapes de construction")|
|:--:|
| <p class="caption">Les différentes étapes de préparation de la dalle (fondations, muret et dalle)</p> |

|![alt text](/images/poly.png "Quatre personnes posant de l'isolant au sol sur un chantier")|
|:--:|
| <p class="caption">Pose de l’isolant sous l’emplacement du futur four</p> |

## Juin : suite de la dalle et plancher de la grange

Après avoir ressenti la joie de pouvoir enfin fouler cette belle dalle quartzée avec nos chaussettes, nous avons vite compris que l’épisode de la dalle n’était pas encore tout à fait terminé. Il a d’abord fallu tout décoffrer et ranger. Ensuite les maçons de l’entreprise de dallage ont disqué des rainures dans la dalle pour prévenir des fissures que nous avons rempli avec des joints en silicone… à plusieurs reprises. Puis nous avons nettoyé, décapé la dalle et passé plusieurs couches d’un vernis bouche-pores à la monobrosse. 

|![alt text](/images/monob.png "personne passant la monobrosse sur une dalle en béton dans une grange en pisé")|
|:--:|
| <p class="caption">Passion monobrosse</p> |

Une fois le plancher des vaches refait à neuf, il s’est agi de s’occuper du plancher des hirondelles : nous nous sommes aussi attaqué∙es à l’étage de la grange. Les solives ont été renforcées et remplacées et les vieilles planches ont été descendues pour refaire un plancher en OSB tout en protégeant les nids d’hirondelles rustiques qui venaient de naitre. Ces précautions n’auront pas été vaines car nous avons la joie depuis quelques jours de voir les petites hirondelles quitter le nid et s’essayer au vol dans la grange. 

|![alt text](/images/hirond.jpg "nid d’hirondelles accroché à une poutre en bois. il est surmonté de trois planches assemblées pour protéger le nid")|
|:--:|
| <p class="caption">Protection du nid pendant les travaux</p> |

En plus des oiseaux, la ferme accueille aussi depuis ce mois-ci de nouveaux amis animaux : les abeilles de Sofia, qui a posé douze ruches en lisière de forêt pour faire du bon miel de châtaigner et de ronce.

|![alt text](/images/sofia.png "deux mains d’une personne en tenue d’apiculture tiennent le cadre d’une ruche. Le cadre est recouvert d’abeilles en train de remplir les alvéoles de miel")|
|:--:|
| <p class="caption">Les abeilles de Sofia en plein travail</p> |

L’arrivée des beaux jours a aussi été l’occasion d’organiser nos premiers chantiers participatifs et notre premier repas-concert à Grenoble pour faire connaitre la ferme. Les chantiers ont été organisés avec l’association Chantiers solidaires 38 : une quinzaine de personnes sont venues sur un weekend nous aider à construire toilettes sèches, escaliers, et bacs de lavage pour nos outils. Merci à elleux !
Et pendant ce temps, le blé et le seigle poussent, les marchés continuent et le chantier du four se prépare.

|![alt text](/images/toil.png "construction extérieure posée sur des palettes et bardée de bois devant un paysage de prairie et de forêt")|
|:--:|
| <p class="caption">Une des réalisations des chantiers participatifs : les toilettes sèches « de luxe »</p> |

|![alt text](/images/seigle.png "épi de seigle dans un champ de seigle. Une coccinelle est posée sur l’épi. On voit les nuages en arrière-plan et le ciel bleu.")|
|:--:|
| <p class="caption">Les seigles attendent les moissons</p> |