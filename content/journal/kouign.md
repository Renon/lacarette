---
title: "Les vents couinent"
date: 2022-11-12T14:33:51+01:00
draft: false
meta_img: "/images/content/kouign/kouign.jpg"
---

L'hiver s'est finalement décidé à entrer dans Rives, et mon fournil de l'accueillir par ses ouvertures généreuses entre ses murs et son toit. S'annonce un jeu du chaud et du souris-pas-trop, où les pâtons se shootent aux infrarouges d'armoires de pousse improvisées. Le taux de levain atteint des sommet, si bien qu'il hydratera bientôt la pâte sans recours à l'eau. Non j'exagère, on arrive à sortir du pain à 10°C, on verra quand on se promènera vers 0°C.

#### Courants d'air et friands

Le débarquement de la clim hivernale indique naturellement la saison de la pâte feuilletée sans beurre tranché, fugant hors des feuillets. De quoi se faire une réserve de pâtes en 6 tours simples, et des friands pour les premiers frimats.

|![alt text](/images/content/kouign/kouign.jpg "Couille Hammam")|
|:--:|
| <p class="caption">Roulés collés caramel</p> |

L'occasion également de suivre la recette de Kouign Amann de la ferme de la Batailleuse (à Mouthe), et d'en déguster le produit. Ça couine de beurre salé, ça grésille de sucre caramélisé et ça s'effeuille en calques dorés. On fait des réserves, des essais et on s'encolle, avant de vous le proposer en proue sur l'étal.


