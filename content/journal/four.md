---
title: "Un four à pain est né"
date: 2021-12-05T14:33:51+01:00
draft: false
meta_img: "/images/content/four/plancher.jpg"
---

Fractio Panis ! Le four du Levain Bullant vient de faire sa première chauffe hier, et a sorti ses premières pizza aujourd'hui ! Retours sur la construction d'un igloo d'acier.

|![alt text](/images/content/four/plancher.jpg "Bricole à l'Atelier Paysan")|
|:--:|
| <p class="caption">Soudure du double fond du four</p> |

#### Des poupées russes en acier

Le four est composé de trois enceintes, de l'intérieur vers l'extérieur : la chambre de cuisson, la chambre de combustion et de passage des fumées et l'enceinte contenant les isolants. C'est un genre de bain-marie isolé, d'igloo-sauna qui mise plus sur l'efficacité d'une combustion en foyer isolé (type rocket) et la réduction des pertes (10cm d'isolants), que sur l'inertie des parois. Tout l'inverse des fours romain en voûte !

|![alt text](/images/content/four/igloo.jpg "Igloo-sauna")|
|:--:|
| <p class="caption">Laine de céramique en première couche d'isolation</p> |

|![alt text](/images/content/four/chapeau.jpg "Chapeau le four")|
|:--:|
| <p class="caption">Laine de roche et enceinte extérieure posée, manque plus que le chapeau !</p> |

#### Le manège à pain

Dans la chambre de cuisson, deux soles circulaires sont soudées à un axe central pour rendre l'enfournement plus aisé et la cuisson plus homogène. Chaque sole reçoit un disque de briques réfractaires qui restitueront leur chaleur aux pain.

|![alt text](/images/content/four/sole.jpg "Aspiration Bosch")|
|:--:|
| <p class="caption">Découpe des briques en cercle de 1m de diamètre au disque diamant</p> |

#### On charge la mule

Le four est sur pieds, maintenant on le veut sur roues. Après une périlleuse manoeuvre pour le faire entrer par l'arrière de la remorque, ses 600 kilos se posent en douceur sur les traverses en acier ajoutées en renfort du plancher. L'igloo se paie même le luxe d'amortisseurs en caoutchouc sous ses pieds délicats pour épargner les briques lors du transport.

|![alt text](/images/content/four/affaire.jpg "Forçat des gravats")|
|:--:|
| <p class="caption">Four monté sur ses traverses en acier, avec parisien intégré à droite !</p> |

La remorque se retrouve avec des étagères intégrés (sur rails) pour que les pains poussent à côté du four. Moins de trajet pour l'enfournement également !
