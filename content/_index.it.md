---
title: "Pagina iniziale"
date: 2024-02-09T14:50:52+01:00
draft: false
---

# Sostenete l'acquisto della nostra fattoria!

Vogliamo rilevare un'azienda agricola a **Saint-Ondras**, un piccolo villaggio vicino Lione, nel sud-est della Francia. Il nostro è un progetto collettivo di **agricoltura biologia e locale**. Dopo più di tre anni di riflessioni, tentativi e progetti, costruzione e de-costruzione, il nostro collettivo si lancia a  capofitto nella concretizzazione di questa straordinaria avventura!

|![alt text](/images/neige.jpg "La ferme des Jalinières sous la neige")|
|:--:|
| <p class="caption">La fattoria di Jallinières sotto la neve</p> |

## Che cos'è?

L'azienda agricola di Jallinières ha interrotto la sua attività di vacche da latte nel 2021 : tutto va trasformato e reinventato. L'**acquisto** è il primo passo verso la creazione delle attività agricole, e di un luogo associativo d'incontri e di scambi (intorno a dei cantieri partecipativi all'inizio, e tante altre cose più tardi).

|![alt text](/images/croquis_gris.jpg "Croquis de la ferme")|
|:--:|
| <p class="caption">Proiezione delle attività della fattoria</p> |

## Chi è?

La prima attività che sarà avviata è la coltivazione di **cereali**, insieme alla **macinazione** e al panificio: Clément e Florentin hanno già seminato i primi ettari quest'autunno! Nel 2024, Emma, Jacob e Nathalie inizieranno a praticare l'**orticoltura** sui tre ettari proprio di fronte alla fattoria. Poi, per sostenere la cultura di **frutti rossi** di Eva e le sue deliziose marmellate, verranno piantate molte siepi e alberi da frutto, in sinergia con l'orto e la biodiversità circostante. Marilou sarà polivalente per aiutare in tutte le attività.

<img class="noborder" src="/images/cycle-transp.png"></img>

## Perché partecipare?

Per favorire l’insediamento di giovani prontə a far parte della **futura generazione** di contadine e contadini: che sfida! Vogliamo creare un luogo di **incontro** e di **scambio**, di organizzazione **collettiva**, di conservazione della **biodiversità** e di sperimentazione dell'**agroecologia**. 

Con l'aiuto delle vostre donazioni e dei vostri prestiti, faremo di questa fattoria un **bene comune**, di proprietà di un'associazione: i nostri successori non dovranno indebitarsi per lavorarci.

## A cosa serviranno i fondi raccolti?

<img class="noborder" src="/images/objectifs.png"></img>

### Prima tappa : 33 000 €

la somma che ci manca per far sì che l'associazione possa acquistare la fattoria, toglierla dal mercato speculativo e perpetuare un'attività agro-ecologica!

### Seconda tappa :  + 14 000 € (per un totale di 47 000 €)

l'associazione può acquisire i 3 ettari previsti per l'attività di orticoltura, assicurandosi il terreno su cui verranno costruite le serre e i cumuli di coltivazione!

### Terza tappa : + 9 000 € (per un totale di 56 000 €)

l'associazione può mettere a norma il sistema fognario dell'azienda, collegandolo alla rete comunale.

### E se abbiamo davvero molto...

l'associazione può allestire una sala riunioni, una cucina, una biblioteca comune e un dormitorio per accogliere visitatori e passanti!

<iframe id="haWidget" allowtransparency="true" src="https://www.helloasso.com/associations/les-ami-es-de-la-carette/collectes/achat-d-une-ferme-et-premiers-travaux/widget-bouton" style="width: 100%; height: 70px; border: none;"></iframe>

**INFO IMPORTANTE**: se desiderate donare più di 7000€ e ricevere un certificato fiscale in modo che il 66% della vostra donazione sia detraibile dalle imposte, contattateci! > amies@la-carette.fr

