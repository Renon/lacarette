---
title: L'associazione
date: 2023-01-01
menu: main
description: Présentation des associations
draft: false
---
Faremo di questa fattoria un **bene comune**, di proprietà di un'associazione: i nostri successori non dovranno **indebitarsi** per lavorarci.

Ispirata da altri collettivi di contadinə che hanno sperimentato questo modello prima di noi, la nostra struttura giuridica si basa su un **modello associativo**. Questo modello fa sì che nessun possa rivendicare la proprietà individuale della fattoria e della terra che la circonda, e sancisce, nello statuto, che la fattoria perseguirà una missione agro-ecologica e di legame sociale, affinché il nostro impegno di proteggerla collettivamente perduri nel tempo!

A tal fine abbiamo costituito l'**associazione "les ami-es de la carette"**, che ha i seguenti obiettivi: 
1. **perpetuare l'agricoltura locale su piccola scala proteggendo i terreni agricoli dalla speculazione immobiliare e dall'economia di mercato**
Vogliamo che i terreni e gli edifici agricoli mantengano la loro vocazione agricola per contribuire all'autosufficienza alimentare del territorio.

2. **Creare un luogo di produzione, trasformazione e commercializzazione agricola rispettoso dell'ambiente**
Abbiamo scelto di partecipare allo sviluppo dell'agricoltura biologica. L'obiettivo è quello di consentire alle agricoltrici e agli agricoltori di vivere del proprio lavoro, contribuendo al contempo a migliorare la biodiversità attraverso pratiche agro-ecologiche.

3. **Promuovere forme di organizzazione collettive e anti-autoritarie**
Vogliamo mettere in pratica forme di vita collettive, solidali, autogestite e attente (??) ai meccanismi di dominio sistemico.

<img class="noborder" src="/images/montage.png"></img>

Per separare la proprietà dall'usufrutto e garantire che la fattoria perduri ben al di là noi, creeremo due associazioni all'interno fattoria: una proprietaria e una gestionaria. A queste si aggiungeranno le società agricole.

- L'**associazione proprietaria** "les ami·es de la carette" riunisce le e i contribuenti che sostengono il progetto con i loro doni o prestiti. Acquisterà gli edifici (al termine di questa campagna di raccolta fondi) e li affitterà all'associazione gestionaria con un contratto di locazione enfiteutica.

- L'**associazione gestionaria** "les passager⋅ères de la carette" sarà responsabile della gestione, dei lavori da realizzare e dell'affitto degli edifici. Affitterà gli edifici e i terreni con contratti di locazione rurale alle strutture agricole.

- Le **strutture agricole** avranno l'usufrutto degli edifici e dei terreni. I loro affitti saranno utilizzati per rimborsare i doni e i prestiti concessi all'associazione proprietaria.

