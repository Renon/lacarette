---
title: L'association
date: 2023-01-01
menu: main
description: Présentation des associations
draft: false
---
Nous voulons faire de la ferme un <b>bien commun</b> : nos successeur⋅es n'auront pas à s'endetter pour y travailler. Nous voulons ainsi rompre le cercle vicieux de l'<b>endettement</b>.

Inspiré·es par d'autres collectifs de paysan·nes qui ont testé ce modèle avant nous, notre montage juridique s'appuie sur un modèle <b>associatif</b> : si personne ne peut réclamer la propriété individuelle de cette ferme et des terres qui l'entourent, s'il est écrit que cette ferme poursuivra une mission agro-écologique et de lien social, nous la protégeons collectivement - et ce dans le temps long !

Pour cela, nous avons monté l'association "les ami·es de la carette", qui porte les objectifs suivants : 

1. Pérenniser l'agriculture paysanne en sortant le foncier agricole de la spéculation immobilière et de l'économie de marché
   Nous voulons que les terres et les bâtis agricoles gardent leur vocation agricole pour participer à l'autonomie alimentaire du territoire, sans créer de dette pour les nouvelles et nouveaux usager·ères.
2. Créer un lieu de production, de transformation et de commercialisation agricole respectueuse de l'environnement
   Nous faisons le choix de participer au développement d'une agriculture paysanne et biologique. L'objectif est de permettre aux paysan·nes de vivre de leur travail tout en contribuant à renforcer la biodiversité au travers de pratiques agro-écologiques.
3. Valoriser des modes d'organisation collectifs et anti-autoritaires 
   Nous souhaitons mettre en pratique des formes de vies collectives, solidaires, autogérées et soucieuses des mécanismes de dominations systémiques.

<img class="noborder" src="/images/montage.png"></img>

Pour séparer la propriété de l'usage et pérenniser la ferme au delà des notre installation à nous, il y aura deux associations sur la ferme : une propriétaire et une gestionnaire. À elles s'ajoutent les structures agricoles.

* L'association propriétaire "les ami·es de la carette" rassemble les contributeur·ices soutenant le projet avec leurs capitaux. Elle achètera les bâtiments et les louera par un bail emphytéotique à l'association gestionnaire.
* L'association gestionnaire “les passager⋅es de la carette” sera responsable de la gestion des lieux, prendra en charge les travaux à réaliser et louera par baux ruraux les bâtiments aux structures agricoles type GAEC (groupement agricole d’exploitation en commun).
* Les deux GAEC (céréales et maraîchage) et autres structures qui pourraient s’installer sur place auront l'usage des bâtiments et des terres. Leurs loyers permettent de rembourser les apports et les prêts faits à l'association propriétaire.